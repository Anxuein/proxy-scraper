from classes import PScraper

pages = 1
timeout = 5
threads = 10
file_to_save = "proxies.txt" 
only_socks = False # ignores HTTP(S)
test_proxies = True # Tests proxies with desired timeout
play_sound = True # Windows only

PScraper.PScraper(pages, timeout, threads, file_to_save, only_socks, test_proxies, play_sound)
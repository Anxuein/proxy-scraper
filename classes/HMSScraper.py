import urllib2
from bs4 import BeautifulSoup
from PTester import *
from PUtils import *

class PHMSScraper:
	proxy_list = []
	config = {}
	
	def __init__(self, configuration):
		self.config = configuration
		self.HMSScraper()
		
	def HMSScraper(self):
		print "Starting to scrape %s pages from HideMyAss" % self.config["pages"]
		self.currentPage = 1
		desiredType = PUtils().getDesiredType(self.config["justSocks"])
		
		while (self.currentPage <= self.config["pages"]):
			source = BeautifulSoup(self.getSource())
			table = source.find('table')
			css = self.HMSGetCSS(table.findAll('style'))
			
			getTrEntries = table.findAll('tr')
			for entry in getTrEntries:
				if entry.get('id') is not None and entry.get('id') == "theader":
					continue
					
				getTd = entry.findAll('td')
				for tdEntry in getTd[1:]:
					for tdChild in tdEntry.children:
						span = getTd[1].find('span')
						for innerSpanElement in span.children:
							if innerSpanElement.name == None:
								continue
								
							if innerSpanElement.name == "style" or innerSpanElement.text == "":
								innerSpanElement.decompose()
								continue
								
							attribName = innerSpanElement.get('class')
							
							try:
								for key, value in css.iteritems():
									if attribName[0] is not None and attribName[0] == key:
										innerSpanElement["style"] = "display:" + value
										if innerSpanElement["style"] == "display:none":
											innerSpanElement.decompose()
							except:
								continue
								
					for tdChild in tdEntry.children:
						spanDec = getTd[1].find('span')
						for innerSpanElement in spanDec.find_all(style='display:none'):
							innerSpanElement.decompose()

			# loop back through to get the clean entries
			last = ''
			getTrEntries = table.findAll('tr')
			for entry in getTrEntries:
				if entry.get('id') is not None and entry.get('id') == "theader":
					continue
				getTd = entry.findAll('td')
				for tdEntry in getTd[1:]:
					for tdChild in tdEntry.children:
						span = getTd[1].find('span')
						
						if last == span.text:
							continue
							
						last = span.text
						port = getTd[2].text
						
						if self.config["justSocks"]:
							if getTd[6].text[:4].lower() == "http":
								continue
								
						self.proxy_list.append("%s:%s" % (last.strip(), port.strip()))
			
		if self.config["testProxies"]:
			print "Scraped %d %s, now testing to see if they are working.." % (len(self.proxy_list), desiredType)
			PUtils().testProxies(self.proxy_list, self.config["threads"], self.config["timeout"], self.config["filename"])
		else:
			print "Scraped %d %s, writing to file" % (len(self.proxy_list), desiredType)
			PUtils().writeList(self.proxy_list, self.config["filename"])
			
	def HMSGetCSS(self, style):
		css = {}
		for theStyle in style:
			for styleLine in theStyle.text.splitlines():
				getClassName = styleLine.split(".")
				if getClassName is not None and len(getClassName) > 1:
					getClassName1 = getClassName[1].split("{")
					if getClassName1[0] is not None and len(getClassName1) > 1:
						getProperty = getClassName1[1].split(":")
						if getProperty[0] is not None and len(getProperty) > 1:
							data = getProperty[1].rstrip()
							if data[len(data)-1] == "}":
								css[getClassName1[0]] = data[:-1]
							else:
								css[getClassName1[0]] = data
		return css
		
	def getSource(self):
		url = "http://hidemyass.com/proxy-list/search-226281/%s"
		opener = urllib2.build_opener()
		opener.addheaders = [('User-agent', PUtils().getUserAgent())]
		self.currentPage += 1
		return opener.open(url % self.currentPage).read()
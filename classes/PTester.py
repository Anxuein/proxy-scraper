import threading
import urllib2
class PTester(threading.Thread):
	theList = []
	returnObject = []
	
	def __init__(self, the_proxy_list, timeout, returnObj):
		threading.Thread.__init__(self)
		self.theList = the_proxy_list
		self.timeout = timeout
		self.returnObject = returnObj
		
	def checkList(self, theList):
		import PUtils
		util = PUtils.PUtils()
		working = []
		for theIP in theList:
			proxy_handler = urllib2.ProxyHandler({'http': "%s" % theIP})
			opener = urllib2.build_opener(proxy_handler, urllib2.HTTPHandler)
			opener.addheaders = [('User-agent', '%s' % util.getUserAgent())]
			urllib2.install_opener(opener)
			req = urllib2.Request('http://www.whatismyip.com/')
			try:
				sock = urllib2.urlopen(req, timeout = self.timeout)
				buf = sock.read(1000)
				if '<title>What Is My IP - The IP Address Experts Since 1999</title>' in buf:
					working.append(theIP)
			except urllib2.HTTPError, e:
				#print e
				continue
			except Exception, e:
				#print e
				continue
				
		return working
			
	def run(self):
		self.returnObject.append(self.checkList(self.theList))
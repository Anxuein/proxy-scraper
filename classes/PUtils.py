import math
import random
from time import localtime, strftime
from PTester import *

class PUtils:
	def setUpLists(self, list, threads):
		amount_threads = float(threads)
		divideLists = int(math.ceil(len(list)/amount_threads))
		
		theLists = [list[x:x+divideLists] for x in xrange(0, len(list), divideLists)]
		return theLists
		
	def writeList(self, proxyList, fileName):
		try:
			fileName = self.successfulFile
		except:
			pass

		finalProxyList = []
		
		if isinstance(proxyList[0], list):
			for proxy in sum(proxyList, []):
				finalProxyList.append(proxy)
		else:
			finalProxyList = proxyList
		
		if len(finalProxyList) == 0:
			print "No proxies available"
			return

		try:
			fopen = open(fileName, "a")
			for proxy in finalProxyList:
				fopen.write("%s\n" % proxy)
			fopen.close()
			self.successfulFile = fileName
			print "Proxies are saved in %s" % fileName
		except:
			print "Unable to use %s, trying another" % fileName
			try:
				theFileName = strftime("proxies_%H%M%S%d%m.txt", localtime())
				fopen = open(theFileName, "a")
				for proxy in finalProxyList:
					fopen.write("%s\n" % proxy)
				fopen.close()
				self.successfulFile = theFileName
				print "Proxies are saved in %s" % theFileName
					
			except:
				print "Unable to create a backup file, printing instead"
				for proxy in finalProxyList:
					print proxy
					
	def testProxies(self, list, threads, timeout, filename):
		theLists = PUtils().setUpLists(list, threads)
		thread_list = []
		working_proxies = []
		amount_working_proxies = int(0)
		
		for list in theLists:
			thread_list.append(PTester(list, timeout, working_proxies))
			thread_list[-1].start()
			
		for thread in thread_list:
			thread.join()
		
		for list in working_proxies:
			amount_working_proxies += len(list)
		
		print "%s working proxies with timeout: %s" % (amount_working_proxies, timeout)
		self.writeList(working_proxies, filename)
		
	def getUserAgent(self):
		userAgents = ["Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1; WOW64; Trident/6.0)", "Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1; Trident/6.0)", "Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1; Trident/6.0)", "Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1; Trident/4.0; InfoPath.2; SV1; .NET CLR 2.0.50727; WOW64)", "Mozilla/5.0 (compatible; MSIE 10.0; Macintosh; Intel Mac OS X 10_7_3; Trident/6.0)", "Mozilla/4.0 (compatible; MSIE 10.0; Windows NT 6.1; Trident/5.0)", "Mozilla/1.22 (compatible; MSIE 10.0; Windows 3.1)", "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:24.0) Gecko/20100101 Firefox/24.0", "Mozilla/5.0 (Windows NT 6.0; WOW64; rv:24.0) Gecko/20100101 Firefox/24.0", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:24.0) Gecko/20100101 Firefox/24.0", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1664.3 Safari/537.36", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_8) AppleWebKit/537.13+ (KHTML, like Gecko) Version/5.1.7 Safari/534.57.2", "Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.0) Opera 12.14", "Mozilla/5.0 (Windows NT 6.0; rv:2.0) Gecko/20100101 Firefox/4.0 Opera 12.14", "Opera/9.80 (Windows NT 6.0) Presto/2.12.388 Version/12.14"]
		return random.choice(userAgents)
		
	def getDesiredType(self, justSocks):
		if justSocks:
			return "socks"
		else:
			return "http(s)"
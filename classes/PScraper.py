from HMSScraper import PHMSScraper
from VPNGeeksScraper import VPNGeeksScraper

class PScraper:
	def __init__(self, pages, timeout = 5, threads = 10, filename = "scraped.txt", justSocks = False, testProxies = False, sound = False, dev = False):
		PConfig = {}
		PConfig["pages"] = pages
		PConfig["timeout"] = timeout
		PConfig["threads"] = threads
		PConfig["filename"] = filename
		PConfig["justSocks"] = justSocks
		PConfig["testProxies"] = testProxies
		PConfig["developer"] = dev
		
		if testProxies and justSocks:
			print "[##] Socks testing is not yet supported [##]"
			PConfig["testProxies"] = False
			
		self.doWork(PConfig)
		
		if sound:
			self.playSound()
		raw_input("\nPress Enter to exit")
		
	def playSound(self):
		import winsound, sys
		if "win" in sys.platform:
			winsound.PlaySound("SystemAterisk", winsound.SND_ALIAS)
		
	def doWork(self, config):
		PHMSScraper(config)
# Usage

python run.py


# Other info

To run this you will need the BeautifulSoup module and Python `2.7` which it's developed on.

Licensed under the DBAD (Dont Be A Dick) Public License: http://www.dbad-license.org/

There will be more proxy sites added later on, please email me@anxuein.com with custom proxy site requests to be added.

# Credits

Anxuein - Source.

CodeX - Threading support.

BeautifulSoup - for the module.